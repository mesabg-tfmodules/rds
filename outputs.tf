output "db_instance" {
  value       = aws_db_instance.db_instance
  description = "RDS Instance created"
}
