# RDS Module

This module is capable to generate a RDS database instance with default needed configuration

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `username` - database username
- `password` - database password
- `port` - database port
- `subnet_ids` - subnets to locate database instance(s)
- `instance_class` - type of db instance (default db.t2.micro)
- `logs` - cloudwatch logs (default ["audit", "error", "general", "slowquery"])
- `backup_retention_days` - number of days to retain backups (default 5)
- `skip_final_snapshot` - skip final snapshot (default false)

Usage
-----

```hcl
module "rds" {
  source                = "git::https://gitlab.com/mesabg-tfmodules/rds.git"

  environment           = "environment"
  name                  = "slugname"

  username              = "username"
  password              = "password"
  port                  = 3306
  subnet_ids            = ["subnet-xxxxx", "subnet-aaaaa"]
  instance_class        = "db.t2.micro"
  logs                  = ["audit", "error", "general", "slowquery"]
  backup_retention_days = 5
  skip_final_snapshot   = true
}
```

Outputs
=======

 - `db_instance` - Created database instance resource


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
