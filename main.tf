terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.0.0"
    }
  }
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name          = var.name
  subnet_ids    = var.subnet_ids

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}

resource "aws_db_instance" "db_instance" {
  identifier                      = var.name
  db_name                         = var.name
  allocated_storage               = var.allocated_storage
  max_allocated_storage           = var.max_allocated_storage
  storage_type                    = "gp2"
  engine                          = "mysql"
  engine_version                  = var.engine_version
  instance_class                  = var.instance_class
  username                        = var.username
  password                        = var.password
  port                            = var.port
  parameter_group_name            = var.parameter_group_name
  db_subnet_group_name            = aws_db_subnet_group.db_subnet_group.name
  enabled_cloudwatch_logs_exports = var.logs
  backup_window                   = "00:00-01:00"
  backup_retention_period         = var.backup_retention_days
  apply_immediately               = true
  delete_automated_backups        = false
  skip_final_snapshot             = var.skip_final_snapshot
  deletion_protection             = true

  tags = {
    Name                          = var.name
    Environment                   = var.environment
  }
}
