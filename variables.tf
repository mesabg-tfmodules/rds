variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "General RDS name"
}

variable "username" {
  type        = string
  description = "Database username"
}

variable "password" {
  type        = string
  description = "Database password"
}

variable "port" {
  type        = number
  description = "Database port"
  default     = 3306
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet identifiers"
}

variable "instance_class" {
  type        = string
  description = "Size of database instance"
  default     = "db.t2.micro"
}

variable "engine_version" {
  type        = string
  description = "Default engine version"
  default     = "8.0.28"
}

variable "parameter_group_name" {
  type        = string
  description = "Default parameter group"
  default     = "default.mysql8.0"
}

variable "logs" {
  type        = list(string)
  description = "Cloudwatch logs"
  default     = ["audit", "error", "general", "slowquery"]
}

variable "backup_retention_days" {
  type        = number
  description = "Number of days to save a backup"
  default     = 5
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Skip or not the final DB snapshot"
  default     = false
}

variable "allocated_storage" {
  type        = number
  description = "Base Allocated Storage"
  default     = 30
}

variable "max_allocated_storage" {
  type        = number
  description = "Max Allocated Storage"
  default     = 40
}
